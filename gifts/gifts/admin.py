from django.contrib import admin

from gifts.gifts.models import Holiday, Gift


class GiftInline(admin.TabularInline):
    model = Gift
    extra = 0
    fk_name = 'holiday'
    fields = ['name', 'order', 'image', 'reserved']
    readonly_fields = ['reserved']


class HolidayAdmin(admin.ModelAdmin):
    list_display = ['name', 'date', 'opened']
    fields = ['name', 'date', 'opened', 'key']
    readonly_fields = ['key']
    inlines = [
        GiftInline,
    ]


admin.site.register(Holiday, HolidayAdmin)
