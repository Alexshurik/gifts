# -*- coding: utf-8 -*-
import hashlib

from django.db import models
from django.utils import timezone
from django.utils.encoding import force_bytes

from gifts.gifts.managers import OpenHolidaysManager


class Holiday(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False,
                            verbose_name='Название')
    date = models.DateField(null=False, blank=False, default=timezone.now,
                            verbose_name='Дата проведения')
    opened = models.BooleanField(
        null=False, blank=True, default=True, verbose_name='Открыт',
        help_text='Может быть просмотрен через сайт')
    key = models.CharField(max_length=32, null=False, blank=False,
                           verbose_name='Ключ доступа')

    # Менеджеры объектов
    # Из-за особенностей Django необходимо при определении новых менеджеров
    # дополнительно прописывать менеджер `objects`
    objects = models.Manager()
    objects_open = OpenHolidaysManager()

    # Описание класса
    # verbose_name - для отображения в админке
    # ordering - сортировка "по умолчанию" во всех выборках и админке.
    # Могут быть описаны и другие свойства: индексы, название таблицы и т.д.:
    # https://docs.djangoproject.com/en/1.7/ref/models/options/
    class Meta:
        verbose_name = 'Праздник'
        verbose_name_plural = 'Праздники'
        ordering = ['date']

    def __str__(self):
        return self.name

    # Переопределяем стандартный метод для сохранения объектов, чтобы в нем
    # также формировался ключ для ссылки
    def save(self, *args, **kwargs):
        super(Holiday, self).save(*args, **kwargs)
        if not self.key:
            self.key = self.__generate_key()
            # Указываем, какие поля хотим сохранить
            self.save(update_fields=['key'])

    def __generate_key(self):
        """Сгенерировать ключ доступа к объекту по ссылке.
        Используется для формирования ссылок, которые можно "отправить другу".
        :return: ключ доступа
        :rtype: str
        """
        # Строка, от которой посчитаем контрольную сумму (которая и будет
        # секретным ключом доступа)
        secret = '%s%s' % (self.pk, timezone.now().timestamp())
        # Вычисление: получаем md5 от строки. Метод md5 возвращает объект,
        # поэтому затем необходимо получить его представление в виде строки,
        # вызвав метод hexdigest.
        return hashlib.md5(force_bytes(secret)).hexdigest()

    # Метод get_absolute_url используется для формирования ссылки на объект
    # в админке. Также удобно использовать и вне админки.
    # Используем декоратор permalink, чтобы не писать каждый раз вызов функции
    # reverse для получения ссылки.
    @models.permalink
    def get_absolute_url(self):
        # Параметры:
        #   1. название view (URL), для которого формируем ссылку
        #   2. значения для подстановки в шаблон ссылки (регулярное выражение)
        return 'gifts:holiday', (self.key, )
    # Название колонки в таблице в админки
    get_absolute_url.short_description = 'Ссылка'

    def human_date(self):
        """Сформировать дату (в удобном для человека виде)
        :rtype: str
        """
        return self.date.strftime('%d.%m.%Y')


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'holiday_{0}/{1}'.format(instance.holiday.id, filename)


class Gift(models.Model):
    holiday = models.ForeignKey(Holiday, null=False, blank=False,
                                verbose_name='Праздник')
    name = models.CharField(max_length=150, null=False, blank=False,
                            verbose_name='Название')
    order = models.IntegerField(default=10, null=False, blank=False,
                                verbose_name='Порядок следования')
    reserved = models.BooleanField(null=False, default=False,
                                   verbose_name='Забронирован')
    reserve_email = models.CharField(max_length=100, null=True, blank=True,
                                     verbose_name='E-mail')
    reserve_name = models.CharField(max_length=100, null=True, blank=True,
                                    verbose_name='Имя')
    image = models.ImageField(upload_to=user_directory_path, blank=True,
                              verbose_name='Изображение')

    class Meta:
        verbose_name = 'Подарок'
        verbose_name_plural = 'Подарки'
        ordering = ['order', 'name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # Если указано имя или email, то подарок считается зарезервированным
        # В зависимости от этого проставляем сответствующую метку
        self.reserved = bool(self.reserve_email or self.reserve_name)
        super(Gift, self).save(*args, **kwargs)
