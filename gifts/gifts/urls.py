from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns(
    'gifts.gifts.views',  # модуль, в котором находятся view

    # URL-адреса конфигурируются с помощью регулярных выражений

    # "?P<param_name>" указывает название параметра, в который будет записано
    # подходящее под регулярное выражение значение

    # name задает название для URL, которое можно использовать для получения
    # ссылок. Таким образом не придется жестко в коде прописывать формыта ссылок
    # и сами ссылки. Необходимо значть название URL и параметры, которые нужны
    # для формирования ссылки.
    url(r'^holiday/(?P<key>[^/]+)$', 'holiday_view', name='holiday'),

    # Можно указывать несколько параметров
    url(r'^holiday/(?P<key>[^/]+)/reserve/(?P<gift_id>\d+)$',
        'reserve_view', name='reserve'),

    url(r'^holiday/(?P<key>[^/]+)/discard/(?P<gift_id>\d+)$',
        'discard_view', name='discard'),
)


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()