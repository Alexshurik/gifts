"""
Django settings for gifts project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os


# Абсолютный путь к проекту.
# Вычисляется относительно дданного файла (settings.py), поэтому необходимо
# подняться на две директории выше
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'c9pg5o@ee%id61*_p!a(+ukk9zttv=cma7at2mvbs+ha7wu^=e'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django_extensions',  # Команды для разработки shell_plus/runserver_plus/...

    'gifts.gifts',  # Разрабатываемое приложение (находится в папке gifts/gifts)
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'gifts.urls'

WSGI_APPLICATION = 'gifts.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'gifts',
        'USER': 'gifts',
        'PASSWORD': 'gifts',
        'HOST': 'localhost',
        'PORT': 5432,
    }
}


# Включить использование часовых поясов
USE_TZ = True
# Часовой пояс, в котором работает наше приложение
TIME_ZONE = 'Europe/Moscow'


# Использование средств локализации и интернационализации.
# https://docs.djangoproject.com/en/1.7/topics/i18n/
LANGUAGE_CODE = 'ru-RU'
USE_I18N = True
USE_L10N = True


# Путь к шаблонам (абсолютный)
TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)


# Статические файлы (CSS, JavaScript, Images)
STATIC_URL = '/static/'

# Путь к директории, в которой они находятся
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

MEDIA_URL = '/media/'

MEDIA_ROOT = (
    os.path.join(BASE_DIR, 'media')
)
