(function ($) {
    // работаем в strict-mode
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
    'use strict';

    // Проверяем наличие формы на странице.
    var $form = $('.js-reserve-form'),
        $formWrapper = $('.js-reserve-form-wrapper');
    if (!$form.length) return;  // Если не нашли, то не выполняем скрипт


    //
    // Инициализируем обработчики событий
    //

    // При клике на ссылку Забронировать/Снять бронь
    $('.is-gifts').on('click', '.js-change', function (e) {
        // 1. Отключаем стандартное действие
        e.preventDefault();

        // 2. Выполняем собственное: отправку на сервер запроса на изменение брони подарка
        var $this = $(e.currentTarget);  // элемент, на который кликнули
        // Располагаем форму после текущего управляющего блока
        $this.closest('.is-manage').after($formWrapper.removeClass('hidden'));
        // Сохраняем в форму информацию о том
        // 1. К какой странице обратиться (ссылка на изменение подарка)
        $form.attr('action', $this.attr('href'));
        // 2. Какое будем выполнять действие
        $('.js-action', $form).val($this.attr('data-action'));
    });


    // Отправка формы на сервер
    $('body').on('submit', '.js-reserve-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: $form.serialize(),
            cache: false
        }).done(function (data) {
            // Формат данных: если есть msg, значит, произошла ошибка.
            // Иначе - все в порядке, приходит новый вид блока с подарком.

            if(data.msg) {
                // Показываем пользователю сообщение
                alert(data.msg);

                // Показывать сообщения с помощью alert-ов -- плохая практика
                // Здесь это сделано просто для примера, чтобы не усложнять приложение
                // Правильный способ: отобразить сообщение рядом с соответствующим полем
                // формы, если оно неверно заполнено, или рядом с самой формой.
            } else {
                // используем класс в качестве селектора, не завязываемся
                // на блоки, которыми сверстано: сейчас это "li", но может
                // измениться на "div", например. Но ничего не придется менять в JS.
                var giftWrapper = $form.closest('.js-gift');
                giftWrapper.html(data.html);
                $formWrapper.addClass('hidden');  // скрываем форму
            }
        }).fail(function () {
            alert('Ошибка при передаче данных. Попробуйте еще раз!')
        });
    });


    // Кнопка отмены действия в форме редактирования
    $form.on('reset', function (e) {
        $formWrapper.addClass('hidden');
    });

}(jQuery));
